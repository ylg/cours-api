#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

int
open_conn(const char *host, const char *serv)
{
  int ecode, sock, found = 0;
  struct addrinfo *res, hints = {
    .ai_flags = AI_ADDRCONFIG,
    .ai_family = AF_UNSPEC,
    .ai_socktype = SOCK_STREAM,
    .ai_protocol = IPPROTO_TCP
  };

  ecode = getaddrinfo(host, serv, &hints, &res);
  if (ecode) {
    fprintf(stderr, "%s(): getaddrinfo(): %s\n", __func__, gai_strerror(ecode));
    return -1;
  }
  for (struct addrinfo *ptr = res; ptr; ptr = ptr->ai_next) {
    sock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (sock < 0)
      continue;
    if (connect(sock, res->ai_addr, res->ai_addrlen) < 0) {
      (void) close(sock);
      continue;
    }
    found = 1;
    break;
  }
  freeaddrinfo(res);
  if (!found) {
    fprintf(
	    stderr, "%s(): can't connect to %s (port %s)\n", __func__,
	    host, serv
	    );
    return -1;
  }
  return sock;
}
