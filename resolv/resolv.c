#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <ns.h>

static int conv_type(const char *);
static void wait_for_reply(int, uint16_t, struct timeval *);
static void print_packet(const char *, size_t, FILE *);
static void print_rr_a(const char *, FILE *);

int
main(int argc, char **argv)
{
  int ecode, sock;
  ns_type_t type;
  ns_hdr_t hdr;
  ns_name_t name;
  ns_qtc_t qtc;
  const char *dn = argv[1];
  const char *t = argv[2];
  struct sockaddr_storage ns;
  socklen_t nslen;
  struct timeval timeout = {.tv_sec=5, .tv_usec=0};

  if (argc < 3) {
    fprintf(stderr, "usage: %s [<@ns>] <dn> <type>\n", argv[0]);
    exit(1);
  }
  if (argv[1][0] == '@') {
    struct addrinfo *res, hints = {
      .ai_flags = AI_NUMERICHOST,
      .ai_family = AF_UNSPEC,
      .ai_socktype = SOCK_DGRAM,
      .ai_protocol = IPPROTO_UDP
    };
    ecode = getaddrinfo(argv[1] + 1, NS_PORT, &hints, &res);
    if (ecode) {
      fprintf(stderr, "%s: invalid IPv4/v6 NS address\n", argv[1] + 1);
      exit(1);
    }
    (void) memcpy(&ns, res->ai_addr, res->ai_addrlen);
    nslen = res->ai_addrlen;
    sock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (sock < 0) {
      fprintf(stderr, "socket(): [%d, %s]\n", errno, strerror(errno));
      freeaddrinfo(res);
      exit(1);
    }
    freeaddrinfo(res);
    dn = argv[2];
    t = argv[3];
  }
  if (!(type = conv_type(t))) {
    fprintf(stderr, "%s: unknown or unsupported type\n", t);
    (void) close(sock);
    exit(1);
  }
  ecode = ns_make_query(dn, type, &hdr, &name, &qtc);
  if (ecode) {
    fprintf(stderr, "ns_make_query() failed: %d\n", ecode);
    (void) close(sock);
    exit(1);
  }
  ecode = ns_send_query(sock, (const struct sockaddr *) &ns, nslen,
			&hdr, &name, &qtc);
  if (ecode) {
    fprintf(stderr, "ns_send_query() failed: %d\n", ecode);
    (void) close(sock);
    exit(1);
  }
  wait_for_reply(sock, hdr.ns_hdr_id, &timeout);
  (void) close(sock);
  exit(0);
}

static int
conv_type(const char *t)
{
  if (!strcasecmp(t, "a"))
      return NS_TYPE_A;
  if (!strcasecmp(t, "ns"))
      return NS_TYPE_NS;
  if (!strcasecmp(t, "cname"))
      return NS_TYPE_CNAME;
  if (!strcasecmp(t, "soa"))
      return NS_TYPE_SOA;
  if (!strcasecmp(t, "ptr"))
      return NS_TYPE_PTR;
  if (!strcasecmp(t, "mx"))
      return NS_TYPE_MX;
  if (!strcasecmp(t, "txt"))
      return NS_TYPE_TXT;
  if (!strcasecmp(t, "AAAA"))
      return NS_TYPE_AAAA;
  return 0;
}

static void
wait_for_reply(int sock, uint16_t id, struct timeval *to)
{
  int ecode;
  fd_set rfds;
  ssize_t rnoc;
  char pkt[512];
  ns_hdr_t *hdr;

  FD_ZERO(&rfds);
  FD_SET(sock, &rfds);
  ecode = select(sock + 1, &rfds, NULL, NULL, to);
  if (ecode < 0) {
    fprintf(
	    stderr, "%s(): select(): [%d, %s]\n", __func__, errno,
	    strerror(errno)
	    );
    exit(1);
  }
  if (!ecode) {
    fprintf(stderr, "no answer from name server\n");
    exit(1);
  }
  rnoc = recvfrom(sock, pkt, sizeof(pkt), 0, NULL, 0);
  if (rnoc < 0) {
    fprintf(
	    stderr, "%s(): recvfrom(): [%d, %s]\n", __func__, errno,
	    strerror(errno)
	    );
    exit(1);
  }
  hdr = (ns_hdr_t *) pkt;
  if (hdr->ns_hdr_id != id) {
    fprintf(stderr, "%s(): unexpected packet\n", __func__);
    exit(1);
  }
  print_packet(pkt, rnoc, stdout);
}

static void
print_packet(const char *pkt, size_t len,  FILE *os)
{
  ns_hdr_t *hdr = (ns_hdr_t *) pkt;
  uint16_t qdc = ntohs(hdr->ns_hdr_qdcount);
  uint16_t anc = ntohs(hdr->ns_hdr_ancount);
  uint16_t nsc = ntohs(hdr->ns_hdr_nscount);
  uint16_t arc = ntohs(hdr->ns_hdr_arcount);
  const char *cur =  pkt + sizeof(*hdr);
  
  if (len < sizeof(ns_hdr_t)) {
    fprintf(stderr, "%s(); packet too short\n", __func__);
    exit(1);
  }
  if (hdr->ns_hdr_rcode) {
    fprintf(stderr, "%s(): rcode error %d\n", __func__, hdr->ns_hdr_rcode);
    exit(1);
  }
  if (hdr->ns_hdr_tc) {
    fprintf(stderr, "%s(); packet is truncated\n", __func__);
    exit(1);
  }
  fprintf(
	  os, "ID: %hu, RA: %d, RCODE: no error\n", ntohs(hdr->ns_hdr_id),
	  hdr->ns_hdr_ra
	  );
  fprintf(os, ">> QUESTION <<\n");
  for (int i = 0; i < qdc; i++) {
    ns_qtc_t *qtc;
    size_t l;

    l = ns_name_print(cur, pkt, os);
    qtc = (ns_qtc_t *) (cur + l);
    fprintf(
	    os, "\tType: %hu\tClass: %hu\n", ntohs(qtc->ns_qtc_type),
	    ntohs(qtc->ns_qtc_class)
	    );
    cur += l + sizeof(ns_qtc_t);
  }
  fprintf(os, ">> ANSWER <<\n");
  for (int i = 0; i < anc; i++) {
    ns_rr_t *rr;
    size_t l;

    l = ns_name_print(cur, pkt, os);
    rr = (ns_rr_t *) (cur + l);
    fprintf(
	    os, "\tType: %hu\tClass: %hu\tTTL: %u\t", ntohs(rr->ns_rr_type),
	    ntohs(rr->ns_rr_class), ntohl(rr->ns_rr_ttl)
	    );
    switch (ntohs(rr->ns_rr_type)) {
    case NS_TYPE_A:
      print_rr_a(cur + l + sizeof(ns_rr_t), os);
      break;
    }
    cur += l + sizeof(ns_rr_t) + ntohs(rr->ns_rr_rdlen);
  }
}

static void
print_rr_a(const char *addr, FILE *os)
{
  char buf[INET_ADDRSTRLEN];

  (void) inet_ntop(AF_INET, addr, buf, sizeof(buf));
  fprintf(os, "A: %s\n", buf);
}
