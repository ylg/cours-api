#ifndef NS_H
#define NS_H

#include <sys/types.h>
#include <stdio.h>
#include <stdint.h>

#define NS_PORT "53"
#define NS_CLASS_IN 1
#define NS_NAME_MAXLEN 255
#define NS_LABEL_MAXLEN 63
#define NS_PTR_MASK 0xc0
#define NS_PTR_CHECK(p) ((*(p) & NS_PTR_MASK) == NS_PTR_MASK)
#define NS_PTR_OFFSET(p) (ntohs(*(uint16_t *) (p)) & ~(NS_PTR_MASK << 8))

typedef enum {
  NSERR_NOERROR,
  NSERR_NOMEMORY,
  NSERR_LBLMAXLEN,
  NSERR_NAMEMAXLEN,
  NSERR_LBLHYPHEN,
  NSERR_LBLINVCHAR,
  NSERR_LBLEMPTY,
  NSERR_SENDQUERY
} ns_err_t;

typedef enum {
  NS_TYPE_A = 1,
  NS_TYPE_NS = 2,
  NS_TYPE_CNAME = 5,
  NS_TYPE_SOA = 6,
  NS_TYPE_PTR = 12,
  NS_TYPE_MX = 15,
  NS_TYPE_TXT = 16,
  NS_TYPE_AAAA = 28
} ns_type_t;

typedef struct {
  char   name_bytes[NS_NAME_MAXLEN];
  size_t name_blen;
} ns_name_t;

typedef struct {
  uint16_t ns_hdr_id;
#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
  uint8_t  ns_hdr_qr:1,
           ns_hdr_opcode:4,
           ns_hdr_aa:1,
           ns_hdr_tc:1,
           ns_hdr_rd:1;
  uint8_t  ns_hdr_ra:1,
           ns_hdr_z:3,
           ns_hdr_rcode:4;
#endif
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
  uint8_t  ns_hdr_rd:1,
           ns_hdr_tc:1,    
           ns_hdr_aa:1,
           ns_hdr_opcode:4,
           ns_hdr_qr:1;
  uint8_t  ns_hdr_rcode:4,
           ns_hdr_z:3,
           ns_hdr_ra:1;
  uint16_t ns_hdr_qdcount;
  uint16_t ns_hdr_ancount;
  uint16_t ns_hdr_nscount;
  uint16_t ns_hdr_arcount;
#endif
} __attribute__((packed)) ns_hdr_t;

typedef struct {
  uint16_t ns_qtc_type;
  uint16_t ns_qtc_class;
} __attribute__((packed)) ns_qtc_t;

typedef struct {
  uint16_t ns_rr_type;
  uint16_t ns_rr_class;
  uint32_t ns_rr_ttl;
  uint16_t ns_rr_rdlen;
} __attribute__((packed)) ns_rr_t;
  
int ns_name_set(const char *, ns_name_t *);
size_t ns_name_print(const char *, const char *, FILE *);
int ns_make_query(const char *, ns_type_t, ns_hdr_t *, ns_name_t *, ns_qtc_t *);
int ns_send_query(
  int, const struct sockaddr *, socklen_t, ns_hdr_t *, ns_name_t *, ns_qtc_t *
		  );

#endif /* NS_H */
