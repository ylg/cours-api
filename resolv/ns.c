#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include <ns.h>

int
ns_name_set(const char *dn, ns_name_t *name)
{
  char *dnd, *r;

  if (!strcmp(dn, ".")) {
    name->name_bytes[0] = 0;
    name->name_blen = 1;
    return NSERR_NOERROR;
  }
  dnd = strdup(dn);
  if (!dnd)
    return NSERR_NOMEMORY;
  name->name_blen = 0;
  for (char *p = dnd, *q = name->name_bytes;; p = r) {
    size_t len;
    
    r = strchr(p, '.');
    if (r)
      *r++ = 0;
    len = strlen(p);
    if (!len) {
      free(dnd);
      return NSERR_LBLEMPTY;
    }
    if (len > NS_LABEL_MAXLEN) {
      free(dnd);
      return NSERR_LBLMAXLEN;
    }
    if (q - name->name_bytes + len + 2 > NS_NAME_MAXLEN) {
      free(dnd);
      return NSERR_NAMEMAXLEN;
    }
    if (p[0] == '-' || p[len -1] == '-') {
      free(dnd);
      return NSERR_LBLHYPHEN;
    }
    for (char *c = p; *c; c++) {
      if (!isalnum(*c) && *c != '-') {
	free(dnd);
	return NSERR_LBLINVCHAR;
      }
      *c = tolower(*c);
    }
    *q++ = (char) len;
    (void) memcpy(q, p, len);
    name->name_blen += len + 1;
    q += len;
    if (!r || !*r) {
      *q = 0;
      name->name_blen++;
      break;
    }
  }
  free(dnd);
  return NSERR_NOERROR;
}

size_t
ns_name_print(const char *name, const char *base, FILE *os)
{
  size_t len = 0;

  for (const char *ptr = name; *ptr; ptr += *ptr + 1) {
    if (NS_PTR_CHECK(ptr)) {
      ptr = base + NS_PTR_OFFSET(ptr);
      (void) ns_name_print(ptr, base, os);
      return len + sizeof(uint16_t);
    }
    for (const char *c = ptr + 1; c - ptr - 1 < *ptr; c++)
      fprintf(os, "%c", *c);
    fprintf(os, ".");
    len += *ptr + 1;
  }
  if (!len++)
    fprintf(os, ".");
  return len;
}

int
ns_make_query(
  const char *dn, ns_type_t type, ns_hdr_t *hdr, ns_name_t *name,
  ns_qtc_t *qtc
  )
{
  int ecode;

  ecode = ns_name_set(dn, name);
  if (ecode != NSERR_NOERROR)
    return ecode;
  (void) memset(hdr, 0, sizeof(*hdr));
  hdr->ns_hdr_id = htons(getpid() & 0xffff);
  hdr->ns_hdr_rd = 1;
  hdr->ns_hdr_qdcount = htons(1);
  qtc->ns_qtc_type = htons(type);
  qtc->ns_qtc_class = htons(NS_CLASS_IN);
  return NSERR_NOERROR;
}

int
ns_send_query(
  int sock, const struct sockaddr *ns, socklen_t nlen, ns_hdr_t *hdr,
  ns_name_t *name, ns_qtc_t *qtc
  )
{
  ssize_t noc;
  struct iovec iov[] = {
    {.iov_base = hdr, .iov_len = sizeof(*hdr)},
    {.iov_base = name->name_bytes, .iov_len = name->name_blen},
    {.iov_base = qtc, .iov_len = sizeof(*qtc)}
  };
  struct msghdr msg = {
    .msg_name = (void *) ns,
    .msg_namelen = nlen,
    .msg_iov = iov,
    .msg_iovlen = sizeof(iov) / sizeof(struct iovec),
    .msg_control = NULL,
    .msg_controllen = 0,
    .msg_flags = 0
  };

  noc = sendmsg(sock, &msg, 0);
  if (noc < 0)
    return NSERR_SENDQUERY;
  if (noc != sizeof(*hdr) + name->name_blen + sizeof(*qtc))
    return NSERR_SENDQUERY;
  return NSERR_NOERROR;
}
