#include <stdio.h>
#include <stdlib.h>

extern int open_conn(const char *, const char *);

int
main(int argc, char **argv)
{
  int sock;
  
  if (argc != 3) {
    fprintf(stderr, "Usage: %s <host> <port>\n", argv[0]);
    exit(1);
  }
  sock = open_conn(argv[1], argv[2]);
  if (sock < 0)
    exit(1);
  exit(0);
}
