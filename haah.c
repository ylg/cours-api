#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

static void print_entry(const struct addrinfo *, int);

int
main(int argc, char **argv)
{
  int ecode, n = 0;
  struct addrinfo *res;
  
  if (argc != 2) {
    fprintf(stderr, "Usage: %s <host>\n", argv[0]);
    exit(1);
  }
  ecode = getaddrinfo(argv[1], NULL, NULL, &res);
  if (ecode) {
    fprintf(stderr, "getaddrinfo(): %s\n", gai_strerror(ecode));
    exit(1);
  }
  for (struct addrinfo *ptr = res; ptr; ptr = ptr->ai_next, n++)
    print_entry(ptr, n);
  freeaddrinfo(res);
  exit(0);
}

static void
print_entry(const struct addrinfo *ai, int n)
{
  char host[NI_MAXHOST] = "???";
  
  fprintf(stdout, "Entry #%d\n", n);
  fprintf(stdout, "  family: ");
  switch (ai->ai_family) {
  case AF_INET:
    fprintf(stdout, "AF_INET\n");
    break;
  case AF_INET6:
    fprintf(stdout, "AF_INET6\n");
    break;  
  default:
    fprintf(stdout, "%d: unexpected family\n", ai->ai_family);
  }
  fprintf(stdout, "  address: ");
  (void) getnameinfo(ai->ai_addr, ai->ai_addrlen, host, sizeof(host), NULL, 0,
		     NI_NUMERICHOST);
  fprintf(stdout, "%s\n", host);
}
