#ifndef RDATE_H
#define RDATE_H

#include <inttypes.h>

#define RDATE_PORT "20000"
#define RDATE_VERSION 0x01

typedef struct {
    uint8_t  rp_vers;
    uint8_t  rp_cmd;
    uint16_t rp_res;
    uint64_t rp_ts;
} __attribute__((packed)) rdate_pkt_t;

typedef enum {
    RDATE_CMD_REQ = 0x01,
    RDATE_CMD_REP
} rdate_cmd_t;

#endif /* RDATE_H */
