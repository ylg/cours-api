#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netdb.h>
#include <syslog.h>
#include <time.h>

#include <urdate.h>

extern uint64_t htonll(uint64_t);

int
main(void)
{
    int sock, ecode;
    struct addrinfo *res, hints = {
	.ai_flags = AI_PASSIVE,
	.ai_family = AF_INET6,
	.ai_socktype = SOCK_DGRAM,
	.ai_protocol = IPPROTO_UDP
    };
    uint64_t ts;

    ecode = getaddrinfo(NULL, RDATE_PORT, &hints, &res);
    if (ecode) {
	fprintf(stderr, "getaddrinfo(): %s\n", gai_strerror(ecode));
	exit(1);
    }
    sock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (sock < 0) {
	freeaddrinfo(res);
	perror("socket");
	exit(1);
    }
    if (bind(sock, res->ai_addr, res->ai_addrlen) < 0) {
	freeaddrinfo(res);
	(void) close(sock);
	perror("bind");
	exit(1);
    }
    freeaddrinfo(res);
    openlog("rdate", LOG_PID, LOG_DAEMON);
    if (daemon(0, 0) < 0) {
	(void) close(sock);
	perror("daemon");
	exit(1);  
    }
    for (;;) {
	ssize_t noc;
	struct sockaddr_storage from;
	socklen_t fromlen = sizeof(from);
	fd_set rfds;
	rdate_pkt_t pkt;
      
	FD_ZERO(&rfds);
	FD_SET(sock, &rfds);
	ecode = select(sock + 1, &rfds, NULL, NULL, NULL);
	if (ecode < 0) {
	    if (errno != EINTR)
		syslog(LOG_ERR, "select(): %m");
	    continue;
	}
	noc = recvfrom(
	    sock, &pkt, sizeof(pkt), 0, (struct sockaddr *) &from, &fromlen);
	if (noc < 0) {
	    syslog(LOG_ERR, "recvfrom(): %m");
	    continue;
	}
	if (noc < sizeof(pkt)) {
	    syslog(LOG_ERR, "recvfrom(): received truncated packet");
	    continue;
	}
	if (pkt.rp_vers != RDATE_VERSION) {
	    syslog(LOG_ERR, "packet: bad version number\n");
	    continue;
	}
	if (pkt.rp_cmd != RDATE_CMD_REQ) {
	    syslog(LOG_ERR, "packet: invalid command\n");
	    continue;
	}
	(void) memset(&pkt, 0, sizeof(pkt));
	pkt.rp_vers = RDATE_VERSION;
	pkt.rp_cmd = RDATE_CMD_REP;
	(void) time((time_t *) &ts);
	pkt.rp_ts = htonll(ts);
	noc = sendto(
	    sock, &pkt, sizeof(pkt), 0, (const struct sockaddr *) &from,
	    fromlen);
	if (noc < 0) {
	    syslog(LOG_ERR, "sendto(): %m");
	    continue;
	}
    }
}
