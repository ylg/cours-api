#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netdb.h>
#include <time.h>

extern uint64_t ntohll(uint64_t);

#include <urdate.h>

int
main(int argc, char **argv)
{
    int ecode, sock;
    ssize_t noc;
    struct addrinfo *res, hints = {
	.ai_flags = 0,
	.ai_family = AF_UNSPEC,
	.ai_socktype = SOCK_DGRAM,
	.ai_protocol = IPPROTO_UDP
    };
    long timeout = 5L;
    uint64_t ts;
    struct timeval tv = {.tv_sec = timeout, .tv_usec = 0};
    rdate_pkt_t pkt;
    fd_set rfds;
    struct sockaddr_storage from;
    socklen_t fromlen = sizeof(from);
    char host[NI_MAXHOST] = "???", serv[NI_MAXSERV] = "???";

    if (argc != 2) {
	fprintf(stderr, "Usage: %s <host>\n", argv[0]);
	exit(1);
    }
    ecode = getaddrinfo(argv[1], RDATE_PORT, &hints, &res);
    if (ecode) {
	fprintf(stderr, "getaddrinfo(): %s\n", gai_strerror(ecode));
	exit(1);
    }
    sock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (sock < 0) {
	perror("socket");
	freeaddrinfo(res);
	exit(1);
    }
    (void) memset(&pkt, 0, sizeof(pkt));
    pkt.rp_vers = RDATE_VERSION;
    pkt.rp_cmd = RDATE_CMD_REQ;
    noc = sendto(sock, &pkt, sizeof(pkt), 0, res->ai_addr, res->ai_addrlen);
    if (noc < 0) {
	perror("sendto");
	freeaddrinfo(res);
	(void) close(sock);
	exit(1);
    }
    freeaddrinfo(res);
    FD_ZERO(&rfds);
    FD_SET(sock, &rfds);
    ecode = select(sock + 1, &rfds, NULL, NULL, &tv);
    if (ecode < 0)  {
	perror("select");
	(void) close(sock);
	exit(1);
    }
    if (!ecode) {
	fprintf(stderr, "No answer from %s (timeout %lds)\n", argv[1], timeout);
	exit(1);
    }
    noc = recvfrom(
	sock, &pkt, sizeof(pkt), 0, (struct sockaddr *) &from, &fromlen);
    if (noc < 0) {
	perror("recvfrom");
	(void) close(sock);
	exit(1);
    }
    if (noc < sizeof(pkt)) {
	fprintf(stderr, "recvfrom(): received truncated packet\n");
	(void) close(sock);
	exit(1);
    }
    if (pkt.rp_vers != RDATE_VERSION) {
	fprintf(stderr, "packet: bad version number\n");
	(void) close(sock);
	exit(1);
    }
    if (pkt.rp_cmd != RDATE_CMD_REP) {
	fprintf(stderr, "packet: invalid command\n");
	(void) close(sock);
	exit(1);
    }
    (void) getnameinfo(
	(const struct sockaddr *) &from, fromlen, host, sizeof(host),
	serv, sizeof(serv), NI_NUMERICHOST | NI_NUMERICSERV
	);
    ts = ntohll(pkt.rp_ts);
    fprintf(
	stdout, "Date/Time on %s (%s, %s): %s", argv[1], host, serv,
	ctime((const time_t *) &ts)
	);
    (void) close(sock);
    exit(0);
}
