#ifndef RF_COMMON_H
#define RF_COMMON_H

#include <stdio.h>
#include <sys/types.h>
#include <limits.h>

#define RF_PORT "20000"
#define RF_CMD_LEN 4

typedef enum {
  RF_CMD_NOOP,
  RF_CMD_SEND,
  RF_CMD_OKAY,
  RF_CMD_SIZE,
  RF_CMD_QUIT,
  RF_CMD_ERNO
} rf_cmd_type_t;

typedef struct {
  rf_cmd_type_t cmd_type;
  union {
    char cmd_fname[PATH_MAX + 1];
    int cmd_erno;
    size_t cmd_size;
  } dummy;
} rf_cmd_t;
#define cmd_fname dummy.cmd_fname
#define cmd_erno dummy.cmd_erno
#define cmd_size dummy.cmd_size

#define RF_BUFSIZE 512

int rf_cmd_parse(char *, size_t, rf_cmd_t *);
void rf_cmd_print(const rf_cmd_t *, FILE *);

#endif /* RF_COMMON_H */
