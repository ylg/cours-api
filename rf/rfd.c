#include <stdlib.h>
#include <syslog.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <rf_common.h>
#include <serv_daemon.h>

static void sfunc(int, struct sockaddr *, socklen_t);
static int rf_read_cmd(int, rf_cmd_type_t *, int, rf_cmd_t *);

int
main(void)
{
  serv_daemon(AF_INET6, RF_PORT, sfunc, "rf");
  exit(0);
}

static void
sfunc(int sock, struct sockaddr *from, socklen_t fromlen)
{
  int fd;
  ssize_t rnoc;
  char buf[RF_CMD_LEN + PATH_MAX];
  char err[128];
  rf_cmd_t cmd;
  struct stat st;

  {
    rf_cmd_type_t cmds[] = {RF_CMD_SEND, RF_CMD_NOOP};
    
    if (rf_read_cmd(sock, cmds, 1, &cmd) < 0)
      return;
  }
  fd = open(cmd.cmd_fname, O_RDONLY);
  if (fd < 0) {
    syslog(LOG_ERR, "can't open file");
    sprintf(err, "ERNO%d", 4);
    (void) send(sock, err, strlen(err), 0);
    return;
  }
  if (fstat(fd, &st) < 0) {
    (void)close(fd);
    syslog(LOG_ERR, "can't read sisze");
    sprintf(err, "ERNO%d", 5);
    (void) send(sock, err, strlen(err), 0);
    return;
  }
  sprintf(err, "SIZE%zu", st.st_size);
  (void) send(sock, err, strlen(err), 0);
  {
    rf_cmd_type_t cmds[] = {RF_CMD_OKAY, RF_CMD_QUIT, RF_CMD_NOOP};

    if (rf_read_cmd(sock, cmds, 1, &cmd) < 0) {
      (void) close(fd);
      return;
    }
  }
  if (cmd.cmd_type == RF_CMD_QUIT) {
    (void) close(fd);
    return;
  }
  for (;;) {
    char fbuf[RF_BUFSIZE];
    
    rnoc = read(fd, fbuf, sizeof(fbuf));
    if (rnoc <= 0)
      break;
    if (send(sock, fbuf, rnoc, 0) < 0)
      break;
  }
  (void) close(fd);
  rnoc = recv(sock, buf, sizeof(buf), 0);
  {
    rf_cmd_type_t cmds[] = {RF_CMD_QUIT, RF_CMD_NOOP};
    
    if (rf_read_cmd(sock, cmds, 0, &cmd) < 0)
      return;
  }
}

static int
rf_read_cmd(int sock, rf_cmd_type_t *cmds, int flg, rf_cmd_t *cmd)
{
  int found = 0;
  ssize_t rnoc;
  char buf[RF_CMD_LEN + PATH_MAX], err[128];

  rnoc = recv(sock, buf, sizeof(buf), 0);
  if (rnoc < 0) {
    syslog(LOG_ERR, "recv: %m");
    if (flg) {
      sprintf(err, "ERNO%d", 1);
      (void) send(sock, err, strlen(err), 0);
    }
    return -1;
  }
  if (rf_cmd_parse(buf, rnoc, cmd) < 0) {
    syslog(LOG_ERR, "parsing command failed");
    if (flg) {
      sprintf(err, "ERNO%d", 2);
      (void) send(sock, err, strlen(err), 0);
    }
    return -1;
  }
  for (rf_cmd_type_t *ptr = cmds; *ptr != RF_CMD_NOOP; ptr++)
    if (cmd->cmd_type == *ptr) {
      found = 1;
      break;
    }
  if (!found) {
    syslog(LOG_ERR, "unexpected command");
    if (flg) {
      sprintf(err, "ERNO%d", 3);
      (void) send(sock, err, strlen(err), 0);
      return -1;
    }
  }
  return 0;
}
