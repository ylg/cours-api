#ifndef SERV_DAEMON_H
#define SERV_DAEMON_H

#include <sys/types.h>
#include <sys/socket.h>

typedef void (*sfunc_t)(int, struct sockaddr *, socklen_t);

void serv_daemon(sa_family_t, const char *, sfunc_t, const char *);

#endif /* SERV_DAEMON_H */
