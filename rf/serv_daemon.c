#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/wait.h>
#include <netdb.h>
#include <syslog.h>
#include <signal.h>

#include <serv_daemon.h>

static void reap_child(int);

void
serv_daemon(
    sa_family_t af, const char *serv, sfunc_t sfunc, const char *logname
    )
{
    int sock, ecode;
    struct addrinfo *res, hints = {
	.ai_flags = AI_PASSIVE,
	.ai_family = af,
	.ai_socktype = SOCK_STREAM,
	.ai_protocol = IPPROTO_TCP
    };
    struct sigaction sa;

    ecode = getaddrinfo(NULL, serv, &hints, &res);
    if (ecode) {
	fprintf(stderr, "getaddrinfo(): %s\n", gai_strerror(ecode));
	exit(1);
    }
    sock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (sock < 0) {
	freeaddrinfo(res);
	perror("socket");
	exit(1);
    }
    if (bind(sock, res->ai_addr, res->ai_addrlen) < 0) {
	freeaddrinfo(res);
	(void) close(sock);
	perror("bind");
	exit(1);
    }
    freeaddrinfo(res);
    (void) memset(&sa, 0, sizeof(sa));
    sa.sa_handler = reap_child;
    if (sigaction(SIGCHLD, &sa, NULL) < 0) {
	(void) close(sock);
	perror("sigaction");
	exit(1);
    }
    if (listen(sock, SOMAXCONN) < 0)  {
	(void) close(sock);
	perror("listen");
	exit(1);
    }
    openlog(logname, LOG_PID, LOG_DAEMON);
    if (daemon(0, 0) < 0) {
	(void) close(sock);
	perror("daemon");
	exit(1);  
    }
    for (;;) {
	int a, f;
	struct sockaddr_storage from;
	socklen_t fromlen = sizeof(from);
      
	a = accept(sock, (struct sockaddr *) &from, &fromlen);
	if (a < 0) {
	    if (errno != EINTR)
		syslog(LOG_ERR, "accept(): %m");
	    continue;
	}
	f = fork();
	if (f < 0) {
	    syslog(LOG_ERR, "fork(): %m");
	    continue;
	}
	if (f > 0)
	    (void) close(a);
	else {
	    (void) close(sock);
	    sfunc(a, (struct sockaddr *) &from, fromlen);
	    (void) close(a);
	    _exit(0);
	}
    }
}

static void
reap_child(int sig)
{
    int status;

    while (waitpid(-1, &status, WNOHANG) > 0);
}
