#include <string.h>
#include <stdlib.h>

#include <rf_common.h>

int
main(int argc, char **argv)
{
  rf_cmd_t cmd;

  if (rf_cmd_parse(argv[1], strlen(argv[1]), &cmd) < 0)
    exit(1);
  rf_cmd_print(&cmd, stdout);
  exit(0);
}
