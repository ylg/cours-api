#include <string.h>
#include <stdlib.h>

#include <rf_common.h>

static const char *rf_cmd_str[] = {
  "NOOP",
  "SEND",
  "OKAY",
  "SIZE",
  "QUIT",
  "ERNO",
  NULL
};

static int rf_cmd_parse_send(char *, size_t, char *);
static long long rf_cmd_parse_ll(char *, size_t);

int
rf_cmd_parse(char *buf, size_t len, rf_cmd_t *cmd)
{
  int found = -1;
  
  if (len < RF_CMD_LEN)
    return -1; /* buffer too short */
  for (const char **ptr = rf_cmd_str; *ptr; ptr++)
    if (!strncasecmp(buf, *ptr, RF_CMD_LEN)) {
      found = ptr - rf_cmd_str;
      break;
    }
  if (found < 0)
    return -1; /* unknown command */
  cmd->cmd_type = found;
  switch (cmd->cmd_type) {
  case RF_CMD_NOOP:
    break; /* should not happen, makes compiler happy */
  case RF_CMD_SEND:
    if (rf_cmd_parse_send(
	  buf + RF_CMD_LEN, len - RF_CMD_LEN, cmd->cmd_fname) < 0)
      return -1; /* path name too long */
    break;
  case RF_CMD_OKAY:
  case RF_CMD_QUIT:
    if (len > RF_CMD_LEN)
      return -1; /* unexpected argument */
    break;
  case RF_CMD_SIZE:
    {
      long long size;
      
      size = rf_cmd_parse_ll(buf + RF_CMD_LEN, len - RF_CMD_LEN);
      if (size < 0)
	return -1; /* invalid file's size */
      cmd->cmd_size = (size_t) size;
    }
    break;
  case RF_CMD_ERNO:
    {
      long long erno;
      
      erno = rf_cmd_parse_ll(buf + RF_CMD_LEN, len - RF_CMD_LEN);
      if (erno < 0 /* || !rf_erno_check((int) errno) */)
	return -1; /* invalid error number */
      cmd->cmd_erno = (int) erno;
    }
    break;
  }
  return 0;
}

void
rf_cmd_print(const rf_cmd_t *cmd, FILE *os)
{
  fprintf(os, "type: %s (%d)\n", rf_cmd_str[cmd->cmd_type], cmd->cmd_type);
  switch (cmd->cmd_type) {
  case RF_CMD_NOOP:
    break;
  case RF_CMD_SEND:
    fprintf(os, "  filename: %s\n", cmd->cmd_fname);
    break;
  case RF_CMD_OKAY:
  case RF_CMD_QUIT:
    break;
  case RF_CMD_SIZE:
    fprintf(os, "  size: %zu\n", cmd->cmd_size);
    break;
  case RF_CMD_ERNO:
    fprintf(os, "  erno: %d\n", cmd->cmd_erno);
    break;
  }
}
  
static int
rf_cmd_parse_send(char *buf, size_t len, char *fname)
{
  if (len > PATH_MAX)
    return -1;
  (void) memset(fname, 0, PATH_MAX + 1);
  (void) memcpy(fname, buf, len);
  return 0;
}

static long long
rf_cmd_parse_ll(char *buf, size_t len)
{
  char ptr[len + 1];
  long long ret;

  ptr[len] = 0;
  (void) memcpy(ptr, buf, len);
  ret = atoll(ptr);
  if (ret <= 0)
    return -1LL;
  return ret;
}
