#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <libgen.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <rf_common.h>

int
main(int argc, char **argv)
{
  int ecode, sock, fd;
  ssize_t rnoc;
  struct addrinfo *res, hints = {
    .ai_flags = 0,
    .ai_family = AF_UNSPEC,
    .ai_socktype = SOCK_STREAM,
    .ai_protocol = IPPROTO_TCP
  };
  char buf[RF_CMD_LEN + PATH_MAX + 1];
  rf_cmd_t cmd;
  
  if (argc != 3) {
    fprintf(stderr, "Usage: %s <host> <file>\n", argv[0]);
    exit(1);
  }
  ecode = getaddrinfo(argv[1], RF_PORT, &hints, &res);
  if (ecode) {
    fprintf(stderr, "%s: getaddrinfo(): %s\n", argv[1], gai_strerror(ecode));
    exit(1);
  }
  sock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
  if (sock < 0) {
    freeaddrinfo(res);
    perror("socket");
    exit(1);
  }
  if (connect(sock, res->ai_addr, res->ai_addrlen) < 0) {
    freeaddrinfo(res);
    (void) close(sock);
    perror("connect");
    exit(1);
  }
  freeaddrinfo(res);
  sprintf(buf, "SEND%s", argv[2]);
  (void) send(sock, buf, strlen(buf), 0);
  rnoc = recv(sock, buf, sizeof(buf), 0);
  buf[rnoc] = 0;
  if (rf_cmd_parse(buf, strlen(buf), &cmd) < 0) {
    (void) close(sock);
    exit(1);
  }
  fd = open(basename(argv[2]), O_CREAT | O_RDWR | O_TRUNC, 0644);
  if (fd < 0) {
    (void) send(sock, "QUIT", 4, 0);
    (void) close(sock);
    exit(1);
  }
  (void) send(sock, "OKAY", 4, 0);
  for (size_t size = 0;;) {
    ssize_t noc;
    char fbuf[RF_BUFSIZE];

    noc = recv(sock, fbuf, sizeof(fbuf), 0);
    if (noc <= 0) {
      (void) send(sock, "QUIT", 4, 0);
      (void) close(fd);
      (void) close(sock);
      exit(1);
    }
    (void) write(fd, fbuf, noc);
    size += noc;
    if (size >= cmd.cmd_size)
      break;
  }
  (void) close(fd);
  (void) close(sock);
  exit(0);
}
