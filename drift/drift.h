#ifndef DRIFT_H
#define DRIFT_H

#include <inttypes.h>

#define DRIFT_PORT "20000"
#define DRIFT_VERSION 0x01

typedef struct {
  uint8_t  rp_vers;
  uint8_t  rp_cmd;
  uint16_t rp_res;
  uint64_t rp_cts1_sec;
  uint64_t rp_cts1_nsec;
  uint64_t rp_sts_sec;
  uint64_t rp_sts_nsec;
  uint64_t rp_cts2_sec;
  uint64_t rp_cts2_nsec;
} __attribute__((packed)) drift_pkt_t;

typedef enum {
  DRIFT_CMD_REQ = 0x01,
  DRIFT_CMD_REP
} drift_cmd_t;

#endif /* DRIFT_H */
