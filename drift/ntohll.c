#include <stdint.h>
#include <arpa/inet.h>

uint64_t
ntohll(uint64_t x)
{
    uint32_t l, h;

    if (ntohl(1) == 1)
	return x;
    l = ntohl(x & 0xffffffff);
    h = ntohl(x >> 32);
    return ((uint64_t) h << 32) | l;
}

uint64_t
htonll(uint64_t x)
{
    return ntohll(x);
}
